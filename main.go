package main

import (
	"context"
	"flag"

	"github.com/apache/beam/sdks/go/pkg/beam"
    "github.com/apache/beam/sdks/go/pkg/beam/x/beamx"
)

func main() {
	flag.Parse()

	p, s := beam.NewPipelineWithRoot()

	_ = s

	if err := beamx.Run(context.Background(), p); err != nil {
		panic(nil)
	}
}
