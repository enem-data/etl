module gitlab.com/enem-data/etl

go 1.12

require (
	github.com/apache/beam v2.15.0+incompatible
	github.com/bramp/morebeam v0.0.0-20181216053424-3c68a24bf61f // indirect
	github.com/gocarina/gocsv v0.0.0-20190821091544-020a928c6f4e
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	go.opencensus.io v0.22.1 // indirect
	golang.org/x/net v0.0.0-20190912160710-24e19bdeb0f2 // indirect
	google.golang.org/api v0.10.0 // indirect
	google.golang.org/grpc v1.23.1 // indirect
)
