package processor

import "github.com/apache/beam/sdks/go/pkg/beam"
import "gitlab.com/enem-data/etl/csvio"
import "reflect"

func init() {
    beam.RegisterType(reflect.TypeOf(RawData{}))
}

type RawData struct {
    StudentId: int `cvs:NU_INSCRICAO`
}

func getFromCsv(s beam.Scope, csvGlobs ...string) {
    raw := csvio.Read(s, reflect.TypeOf(RawData{}), ...csvGlobs)
    s = s.Scope("Cleaning data")
}
